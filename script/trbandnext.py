#!/usr/bin/python3

import numpy as np
import time
import epics
import threading

def main():
  print(f"[{time.asctime()}] trband next start...")

  #Events
  eve = threading.Event()

  def onchange(*args, **kwargs):
    if kwargs["value"]>0:
      eve.set()

  # connect to epics channels
  print("Connecting epics channels")
  nextpv = topv = epics.PV("LPQ:TRBAND:next", auto_monitor=True, callback=onchange)
  trpv = epics.PV("LPQ:source:axis:1:pos_units_RBV", auto_monitor=True)
  topv = epics.PV("LPQ:TRBAND:to", auto_monitor=True)
  safepv = epics.PV("LPQ:TRBAND:safe", auto_monitor=True)
  trstartpv = epics.PV("LPQ:source:proc_start", auto_monitor=False)
  trstoppv = epics.PV("LPQ:source:proc_stop", auto_monitor=False)
  trpospv = epics.PV("LPQ:source:axis:1:pos_abs", auto_monitor=False)
  trbusypv = epics.PV("LPQ:source:global:moving", auto_monitor=True)
  time.sleep(2)

  #main loop
  print("Listening...")
  eve.clear()
  nextpv.put(0)
  while True:
    try:
      eve.wait()
      if safepv.value>0:
        print(f"[{time.asctime()}] next process startet")
        trstartpv.put(1)
        print("waiting for safe indicator to be off")
        while safepv.value>0:
          time.sleep(0.5)
        print("Stopping TR")
        trstoppv.put(1)
        time.sleep(2)
        print(f"Sending to desired position. to:{topv.value}")
        trpospv.put(topv.value+0.1)
        time.sleep(1.5)
        print("waiting for moving indicator to be off")
        while trbusypv.value>0:
          time.sleep(0.5)
        print("done")
      nextpv.put(0)
      eve.clear()
    except Exception as err:
      print(f"[{time.asctime()}] Exception:")
      print(err)
      time.sleep(5)
  #end of main

# Main call
main()
print("OK")
