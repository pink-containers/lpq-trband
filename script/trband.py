#!/usr/bin/python3

import numpy as np
import time
import epics
import threading

def main():
  print(f"[{time.asctime()}] trband start...")

  #Events
  eve = threading.Event()

  def onchange(*args, **kwargs):
    eve.set()

  def calcrange(length):
    t = (length/100.0)+0.7
    r = (4.5873*t)-0.7440
    return r

  # connect to epics channels
  print("Connecting epics channels")
  trpv = epics.PV("LPQ:source:axis:1:pos_units_RBV", auto_monitor=True, callback=onchange)
  burstpv = epics.PV("LPQ:laser:probe:BurstLength_RBV", auto_monitor=True, callback=onchange)
  #burstpv = epics.PV("LPQ:pysup:aux:A10", auto_monitor=True, callback=onchange)
  frompv = epics.PV("LPQ:TRBAND:from", auto_monitor=True, callback=onchange)
  topv = epics.PV("LPQ:TRBAND:to", auto_monitor=True, callback=onchange)
  safepv = epics.PV("LPQ:TRBAND:safe", auto_monitor=False)
  enablepv = epics.PV("LPQ:TRBAND:enable", auto_monitor=True)
  time.sleep(2)

  #main loop
  print("Listening...")
  eve.clear()
  oldstate=False
  while True:
    try:
      eve.wait()
      if int(enablepv.value)==0:
        newstate = False
      else:
        b1o = frompv.value
        b2o = topv.value
        offset = 180-b1o
        b1 = (b1o+offset)%360
        b2 = (b2o+offset)%360
        #print(f"\nb1:{b1}, b2:{b2}, offset:{offset}")
        t1o = trpv.value
        r = calcrange(burstpv.value)
        t2o = t1o + r
        t1 = (t1o+offset)%360
        t2 = (t2o+offset)%360
        #print(f"t1:{t1}, r:{r}, t2:{t2}")

        cond1 = ((t1>b1) and (t1<b2))
        cond2 = ((t2>b1) and (t2<b2))
        cond3 = ((t1<b1) and (t2>b2))
        newstate = (cond1 or cond2 or cond3)
        #print(f"c1:{cond1}, c2:{cond2}, c3:{cond3}")

      if newstate!=oldstate:
        oldstate=newstate
        safepv.put(int(newstate))

      eve.clear()

    except Exception as err:
      print(f"[{time.asctime()}] Exception:")
      print(err)
      time.sleep(5)

  #end of main

# Main call
main()
print("OK")
